Changelog
=========


(unreleased)
------------

Bug fixes
~~~~~~~~~
- Add missing migration for new plugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.1 (2023-12-12)
-------------------

Bug fixes
~~~~~~~~~
- Agenda archive plugin reverse order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.0 (2023-12-11)
-------------------

Features
~~~~~~~~
- Agenda archive view reverse order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.4.0 (2023-11-27)
-------------------

Features
~~~~~~~~
- Add plugin to display past events [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.8 (2023-11-13)
-------------------

Bug fixes
~~~~~~~~~
- Add missing migration for proxy model (and permissions) [Adrien
  Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.7 (2023-11-13)
-------------------

Bug fixes
~~~~~~~~~
- Archive view queryset [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.6 (2023-06-16)
-------------------

Bug fixes
~~~~~~~~~
- Handle agenda category and tag views [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.5 (2023-05-25)
-------------------

Bug fixes
~~~~~~~~~
- Translation on UpcomingEventsPlugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.4 (2023-05-25)
-------------------

Bug fixes
~~~~~~~~~
- Improve admin filters [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.3 (2023-05-24)
-------------------

Bug fixes
~~~~~~~~~
- Improve admin list display [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.2 (2023-04-17)
-------------------

Bug fixes
~~~~~~~~~
- Agenda querysets filters and order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.1 (2023-04-17)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.0 (2023-04-17)
-------------------

Features
~~~~~~~~
- Add AgendaUpcomingEntriesPlugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.2.2 (2023-04-17)
-------------------

Bug fixes
~~~~~~~~~
- Translations [Adrien Delhorme]
- Patched_urls don't need to rebuild detail url [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update CHANGELOG [Adrien Delhorme]


v0.2.1 (2023-04-04)
-------------------

Bug fixes
~~~~~~~~~
- Better name for admin class [Adrien Delhorme]
- Use timezone aware now() function [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.2.0 (2023-03-30)
-------------------

Features
~~~~~~~~
- Add a field to handle events' end dates [Adrien Delhorme]
- Add working agenda app [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update README [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Upgrade pre-commit, complete .gitignore [Adrien Delhorme]

  And set .python-version to 3.1

- Increase readability, hide featured date from layout [Corentin
  Bettiol]




.. Generated by gitchangelog
