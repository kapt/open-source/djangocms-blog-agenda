Changelog
=========


v0.12.0 (2025-03-10)
--------------------

Features
~~~~~~~~
- Handle is_pinned on events [Adrien Delhorme]
- Allow recurrence disabling [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]

Maintenance
~~~~~~~~~~~
- Fix test config [Adrien Delhorme]


v0.11.2 (2025-01-23)
--------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]

Maintenance
~~~~~~~~~~~
- Move tests directory inside djangocms_blog_agenda package [Adrien
  Delhorme]

Tests
~~~~~
- Add a test case for ongoing events [Adrien Delhorme]


v0.11.1 (2025-01-23)
--------------------

Bug fixes
~~~~~~~~~
- Keep posts without recurrences when calling `add_recurrent_posts`
  [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update readme [Corentin Bettiol]
- Update changelog [Corentin Bettiol]


v0.11.0 (2025-01-15)
--------------------

Features
~~~~~~~~
- Add DJANGOCMS_BLOG_AGENDA_TEMPLATE_PREFIXES [Corentin Bettiol]

  - allow using other template prefixes than the default "djangocms_blog_agenda" (without hiding the "event info" tab in new/edit blog post view


Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.10.0 (2024-12-09)
--------------------

Features
~~~~~~~~
- Add testing framework and configuration files; implement initial tests
  for agenda views [Adrien Delhorme]

  - Introduced .coveragerc for coverage configuration.
  - Added pytest.ini and tox.ini for testing setup.
  - Created requirements-test.txt for test dependencies.
  - Updated setup.cfg to include test dependencies and authors.
  - Implemented initial test cases for AgendaListView and AgendaArchiveView.
  - Added test settings and fixtures for Django testing environment.
  - Enhanced PostExtension model to copy translated fields during instance creation


Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.9.4 (2024-12-09)
-------------------

Bug fixes
~~~~~~~~~
- Initial event condition [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.9.3 (2024-12-06)
-------------------

Bug fixes
~~~~~~~~~
- Recurrences now contain initial event date [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.9.2 (2024-10-24)
-------------------

Bug fixes
~~~~~~~~~
- Condition when `after` is None [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.9.1 (2024-10-23)
-------------------

Bug fixes
~~~~~~~~~
- Add_recurrent_posts keep initial post condition [Adrien Delhorme]
- Fill_recurrences_end_date when no occurrences [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.9.0 (2024-10-15)
-------------------

Features
~~~~~~~~
- Handle recurring events [Adrien Delhorme]

Bug fixes
~~~~~~~~~
- Improve archive plugins templates [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.8.1 (2024-10-07)
-------------------

Bug fixes
~~~~~~~~~
- Reference to a non existent migration [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.8.0 (2024-10-07)
-------------------

Features
~~~~~~~~
- Handle hiding of events after a custom duration [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.7.4 (2024-09-23)
-------------------

Bug fixes
~~~~~~~~~
- Translations [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Corentin Bettiol]


v0.7.3 (2024-09-17)
-------------------

Bug fixes
~~~~~~~~~
- Fix AgendaArchiveView [Corentin Bettiol]

  - update logic to show posts like this: "(start_date <= month && end_date >= month) OR (start_date == month)"
  (previous filter was not including posts which had end_date = None

- Handle case where there's no end date in post blog [Corentin Bettiol]

  - DateCountMixin is relying on d2 (post.extension.event_end_date), but event_end_date is not mandatory. Handle this case


Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.7.2 (2024-09-09)
-------------------

Bug fixes
~~~~~~~~~
- Inject get_sites method on User model when MULTISITE is enabled
  [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.7.1 (2024-06-20)
-------------------

Bug fixes
~~~~~~~~~
- List view must show only upcoming events [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.7.0 (2024-05-30)
-------------------

Features
~~~~~~~~
- Allow template override of items counters [Adrien Delhorme]
- Better handling of items counters in categories, tags and archive
  plugins [Adrien Delhorme]
- Add category filter in admin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.6.2 (2024-05-22)
-------------------

Bug fixes
~~~~~~~~~
- Cmsplugins must inherit directly from CMSPlugin class [Adrien
  Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.6.1 (2024-05-22)
-------------------

Bug fixes
~~~~~~~~~
- Remove inexistent migration reference [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.6.0 (2024-05-22)
-------------------

Features
~~~~~~~~
- Add a plugin option to show events until their end date [Adrien
  Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.6 (2024-04-18)
-------------------

Bug fixes
~~~~~~~~~
- Do not hide all inline instances [Adrien Delhorme]

  Hide agenda extension admin inlines when the config is a djangocms_blog_agenda config

- Update gitchangelog.rc [François PALMIER]

Documentation
~~~~~~~~~~~~~
- Update changelog [François PALMIER]


v0.5.5 (2024-02-06)
-------------------

Features
~~~~~~~~
- Accept POST method on agenda detail view [François PALMIER]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.4 (2023-12-20)
-------------------

Bug fixes
~~~~~~~~~
- AgendaArchiveView only for agenda [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.3 (2023-12-12)
-------------------

Bug fixes
~~~~~~~~~
- Wrong migration reference [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.2 (2023-12-12)
-------------------

Bug fixes
~~~~~~~~~
- Add missing migration for new plugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]
- Update changelog [Adrien Delhorme]


v0.5.1 (2023-12-12)
-------------------

Bug fixes
~~~~~~~~~
- Agenda archive plugin reverse order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.5.0 (2023-12-11)
-------------------

Features
~~~~~~~~
- Agenda archive view reverse order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.4.0 (2023-11-27)
-------------------

Features
~~~~~~~~
- Add plugin to display past events [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.8 (2023-11-13)
-------------------

Bug fixes
~~~~~~~~~
- Add missing migration for proxy model (and permissions) [Adrien
  Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.7 (2023-11-13)
-------------------

Bug fixes
~~~~~~~~~
- Archive view queryset [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.6 (2023-06-16)
-------------------

Bug fixes
~~~~~~~~~
- Handle agenda category and tag views [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.5 (2023-05-25)
-------------------

Bug fixes
~~~~~~~~~
- Translation on UpcomingEventsPlugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.4 (2023-05-25)
-------------------

Bug fixes
~~~~~~~~~
- Improve admin filters [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.3 (2023-05-24)
-------------------

Bug fixes
~~~~~~~~~
- Improve admin list display [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.2 (2023-04-17)
-------------------

Bug fixes
~~~~~~~~~
- Agenda querysets filters and order [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.1 (2023-04-17)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.3.0 (2023-04-17)
-------------------

Features
~~~~~~~~
- Add AgendaUpcomingEntriesPlugin [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.2.2 (2023-04-17)
-------------------

Bug fixes
~~~~~~~~~
- Translations [Adrien Delhorme]
- Patched_urls don't need to rebuild detail url [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update CHANGELOG [Adrien Delhorme]


v0.2.1 (2023-04-04)
-------------------

Bug fixes
~~~~~~~~~
- Better name for admin class [Adrien Delhorme]
- Use timezone aware now() function [Adrien Delhorme]

Documentation
~~~~~~~~~~~~~
- Update changelog [Adrien Delhorme]


v0.2.0 (2023-03-30)
-------------------

Features
~~~~~~~~
- Add a field to handle events' end dates [Adrien Delhorme]
- Add working agenda app [Corentin Bettiol]

Documentation
~~~~~~~~~~~~~
- Update README [Corentin Bettiol]

Maintenance
~~~~~~~~~~~
- Upgrade pre-commit, complete .gitignore [Adrien Delhorme]

  And set .python-version to 3.1

- Increase readability, hide featured date from layout [Corentin
  Bettiol]




.. Generated by gitchangelog
